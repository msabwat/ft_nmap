NAME=ft_nmap

FLAGS=-Wall -Wextra -Werror

CC=clang

INC=-I.

LIBFT_PATH=libft/

LIBFT_INC=-I$(LIBFT_PATH)

LIBFT_LINK_FLAGS=-L $(LIBFT_PATH) -lft

# discovery.c
SRC_NAME=	utils.c \
			options.c \
			tasks.c \
			scan.c


OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

TEST=

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g -fsanitize=address,undefined
test: TEST = test
test: makedir $(NAME)
	echo "testing"

$(NAME): $(OBJ_PATH)/main.o $(OBJS)
	$(MAKE) -C $(LIBFT_PATH)
	$(CC) $(FLAGS) $(OBJ_PATH)/main.o $(LIBFT_INC) $(OBJS) $(INC) $(LIBFT_LINK_FLAGS) -lpthread -lpcap -o $(NAME)

$(OBJ_PATH)/main.o: main.c
	 $(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c main.c -o $(OBJ_PATH)/main.o 
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(NAME)

fclean: clean
	rm -fr $(OBJ_PATH)
	rm -fr main.o
	$(MAKE) fclean -C $(LIBFT_PATH)

re: fclean all
