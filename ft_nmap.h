#include <sys/poll.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pcap.h>
#include <stdbool.h>
#include <libft.h>

typedef struct s_scan t_scan;

typedef struct s_pseudo_header
{
	unsigned int source_address;
	unsigned int dest_address;
	unsigned char placeholder;
	unsigned char protocol;
	unsigned short tcp_length;
 
	struct tcphdr tcp;
} t_pseudo_header;

typedef enum e_scan_types {
	SYN_SCAN = 1,
	NULL_SCAN = 1 << 1,
	ACK_SCAN = 1 << 2,
	FIN_SCAN = 1 << 3,
	XMAS_SCAN = 1 << 4,
	UDP_SCAN = 1 << 5
} t_scan_types;

typedef enum e_status {
	INVALID,
	OPEN,
	CLOSED,
	FILTERED,
	UNFILTERED,
	OPEN_OR_FILTERED
} t_status;

typedef struct s_result {
	t_scan_types scan_type;
	t_status status;
	char *service_name;
} t_result;

typedef struct s_sys {
	pcap_t *capture;
	unsigned char speed;
	unsigned int ports[1024];
	int ports_num;

	struct sockaddr_in *ip_targets;
	int ip_num;

	t_scan_types scan_types;
	int scan_types_num;

	int scan_num;
	t_scan *scans;

	int task_count;
	pthread_mutex_t m_task_count;

	char src_ip[16];
	pthread_t *ids;
	bool flag_retransmit;

	int current_dispatch;
} t_sys;

typedef struct s_scan {
	struct sockaddr_in dst_ip;
	unsigned int dst_port;
	t_result *results;
} t_scan;

int get_options(t_sys *sys, int ac, char **av);
void print_scanconf(t_sys *sys);
// int discover_hosts(t_sys *sys);
int init_scan(t_sys *sys);
int get_local_ip(char *src_ip);
unsigned short checksum(unsigned short *ptr, int nbytes);
int send_scan_pkt(t_sys *sys, t_scan *scan, unsigned int src_port, t_scan_types scan_type);
int process_reply(t_sys *sys, t_scan *scan);
int set_pcap_filter(t_sys *sys);
char *str_from_scan_type(t_scan_types type);
char *str_from_result(t_status status);
void add_result(t_sys *sys, int protocol, unsigned long ip, unsigned int port, const u_char *pkt);
void *syn_task_runner(void *args);
void *fin_task_runner(void *args);
void *ack_task_runner(void *args);
void *null_task_runner(void *args);
void *xmas_task_runner(void *args);
void *udp_task_runner(void *args);
void print_openports(t_sys *sys, int ip_index);
void print_closedports(t_sys *sys, int ip_index);
void free_sys(t_sys *sys);
void free_strsplit(char **str);
