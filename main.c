#include "ft_nmap.h"
#include "libft.h"
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <pcap/sll.h>

#include <signal.h>
#include <netinet/ip.h>

pcap_t *capture = NULL;

void handler (int sig, siginfo_t *siginfo, void *context) {
	(void)sig, (void) siginfo, (void) context;
	// we got here without updating t_result, we already know we timedout 
	pcap_breakloop(capture);
}

static void packet_handler (u_char *args, const struct pcap_pkthdr *packet_header, const u_char *packet_body) {
	(void)packet_header,(void)args;
	t_sys *sys = (t_sys *)args;
	struct iphdr *ip;
	// if the capture is on "any", the kernel will add a linux cooked capture instead of the ETH header
	ip = (struct iphdr *) (packet_body + sizeof(struct sll_header));
	struct tcphdr *tcp = (struct tcphdr *)(packet_body + sizeof(struct sll_header) + (ip->ihl * 4));

	add_result(sys, ip->protocol, ip->saddr, ntohs(tcp->source), packet_body);
	return ;
}

int main(int ac, char **av) {
	struct timeval time;
    long start_time = 0;
    long cur_time = 0;

	uid_t uid = getuid();
	if (uid != 0) {
		printf("ft_nmap: uid not root, need root to use raw sockets \n");
		exit (1);
	}

	t_sys *sys = (t_sys *) malloc(sizeof(t_sys));
	if (!sys)
		exit(1);
	// will wait 3 second max (not min) after the first packet captured
	char error_buffer[PCAP_ERRBUF_SIZE];
	sys->capture = pcap_open_live("any", BUFSIZ, 0, 3000, error_buffer);
	if (!sys->capture) {
		printf("Could not open %s - %s\n", "any", error_buffer);
		exit(1);
	}
	capture = sys->capture;
	sys->scan_types = 0;
	sys->ip_num = 0;
	sys->ports_num = 0;
	sys->scan_types_num = 0;
	sys->scan_num = 0;
	sys->ip_targets = 0;
	sys->speed = 0;
	sys->flag_retransmit = 0;
	sys->ip_targets = NULL;
	sys->scans = NULL;
	sys->ids = NULL;
	for (int i = 0; i < 1024; i++) {
		sys->ports[i] = 0;
	}

	int ret = get_options(sys, ac, av);
	if (!ret) {
		free_sys(sys);
		exit(1);
	}
	if (sys->scan_types == 0) {
		sys->scan_types = SYN_SCAN | NULL_SCAN | FIN_SCAN | UDP_SCAN | XMAS_SCAN | ACK_SCAN;
	}
	if (sys->ports[0] == 0) {
		for (int i = 0; i < 1024; i++) {
			sys->ports[i] = i + 1;
		}
	}
	print_scanconf(sys);
	sys->scan_num = sys->ip_num * sys->ports_num;

	// prepare the scan
	if (!init_scan(sys)) {
		printf("error: could not init scan()\n");
		free_sys(sys);
		exit (1);
	}

	if (!get_local_ip( sys->src_ip )) {
		printf("error could not get local ip\n");
		free_sys(sys);
		exit (1);
	}

	set_pcap_filter(sys);
	if (!sys->capture) {
		free_sys(sys);
		exit (1);
	}

	struct sigaction act;
	ft_memset (&act, '\0', sizeof(act));
	act.sa_sigaction = &handler;
	act.sa_flags = SA_SIGINFO;

	if (sigaction(SIGALRM, &act, NULL) < 0) {
		perror ("sigaction");
		free_sys(sys);
		exit (1);
	}

	// fire up the scan
	printf("Scanning... \n");
	gettimeofday(&time, NULL);
    start_time = time.tv_sec * 1000000 + time.tv_usec;
	int count = 1;
	if (!sys->speed) {
		// TODO: handle max port
		for (int j = 0; j < sys->scan_types_num; j++) {
			for (int i = 0; i < sys->scan_num; i++) {
				send_scan_pkt(sys, &sys->scans[i], count++, sys->scans[i].results[j].scan_type);
			}
			alarm(4);
			sys->current_dispatch = j;
			int ret = pcap_dispatch(sys->capture, -1, packet_handler, (u_char *)sys);
			if (ret == PCAP_ERROR) {
				printf("pcap_dispatch failed : %s\n", pcap_geterr(sys->capture));
				free_sys(sys);
				exit(1);
			}
		}
	}
	else {
		// TODO: // pcap_dispatch
		// now can only dispatch after I sent all the scan_type packets
		void *(*pf_runners[sys->scan_types_num])(void *);
		t_scan_types types = sys->scan_types;
		for (int k = 0; k < sys->scan_types_num; k++) {
			if (((types & SYN_SCAN) & SYN_SCAN) == SYN_SCAN) {
				pf_runners[k] = syn_task_runner;
				types -= SYN_SCAN;
			}
			else if (((types & NULL_SCAN) & NULL_SCAN) == NULL_SCAN) {
				pf_runners[k] = null_task_runner;
				types -= NULL_SCAN;
			}
			else if (((types & FIN_SCAN) & FIN_SCAN) == FIN_SCAN) {
				pf_runners[k] = fin_task_runner;
				types -= FIN_SCAN;
			}
			else if (((types & UDP_SCAN) & UDP_SCAN) == UDP_SCAN) {
				pf_runners[k] = udp_task_runner;
				types -= UDP_SCAN;
			}
			else if (((types & ACK_SCAN) & ACK_SCAN) == ACK_SCAN) {
				pf_runners[k] = ack_task_runner;
				types -= ACK_SCAN;
			}
			else if (((types & XMAS_SCAN) & XMAS_SCAN) == XMAS_SCAN) {
				pf_runners[k] = xmas_task_runner;
				types -= XMAS_SCAN;
			}
		}
		for (int j = 0; j < sys->scan_types_num; j++) {
			for (int i = 0; i < sys->speed; i++) {
				if (pthread_create(&(sys->ids[i]), NULL, pf_runners[j], sys) != 0) {
					free_sys(sys);
					exit(1);
				}
				
			}
			for (int i = 0; i < sys->speed; i++) {
				if (pthread_join(sys->ids[i], NULL) != 0) {
					free_sys(sys);
					exit(1);
				}
			}
			alarm(4);
			sys->current_dispatch = j;
			int ret = pcap_dispatch(sys->capture, -1, packet_handler, (u_char *)sys);
			if (ret == PCAP_ERROR) {
				printf("pcap_dispatch failed : %s\n", pcap_geterr(sys->capture));
				free_sys(sys);
				exit(1);
			}
			sys->task_count = sys->scan_num;
		}
	}
	int retransmit = 0;
	for (int j = 0; j < sys->scan_types_num; j++) {
		retransmit = 0;
		for (int i = 0; i < sys->scan_num; i++) {
			if (sys->scans[i].results[j].status == INVALID) {
				send_scan_pkt(sys, &sys->scans[i], count++, sys->scans[i].results[j].scan_type);
				retransmit = 1;
			}
		}
		if (retransmit) {
			alarm(4);
			sys->current_dispatch = j;
			int ret = pcap_dispatch(sys->capture, -1, packet_handler, (u_char *)sys);
			if (ret == PCAP_ERROR) {
				printf("pcap_dispatch failed : %s\n", pcap_geterr(sys->capture));
				free_sys(sys);
				exit(1);
			}
		}
	}
	for (int j = 0; j < sys->scan_types_num; j++) {
		for (int i = 0; i < sys->scan_num; i++) {
			if (sys->scans[i].results[j].status == INVALID) {
				if ((sys->scans[i].results[j].scan_type == SYN_SCAN)
					|| (sys->scans[i].results[j].scan_type == ACK_SCAN)) {
					sys->scans[i].results[j].status = FILTERED;
					printf(".");
				}
				else {
					sys->scans[i].results[j].status = OPEN_OR_FILTERED;
					printf(".");
				}
			}
			fflush(stdout);
		}
	}	
	printf("\n");
	gettimeofday(&time, NULL);
	cur_time = (time.tv_sec * 1000000 + time.tv_usec) - start_time;
	printf("Scan took %ld.%03ld seconds\n", cur_time / 1000000l, cur_time % 1000l);

	for (int i = 0; i < sys->ip_num; i++) {
		printf("IP Address: %s\n", inet_ntoa(sys->ip_targets[i].sin_addr));
		// open ports	
		printf("Open ports: \n");
		printf("%-5s | %-20s | %-20s | %-10s\n", "Port", "Service", "Scan Type / Result", "Conclusion");
		printf("----------------------------------------------------------------------\n");
		print_openports(sys, i);
		printf("\n");
		printf("Closed/Filtered/Unfiltered ports: \n");
		printf("%-5s | %-20s | %-20s | %-10s\n", "Port", "Service", "Scan Type / Result", "Conclusion");
		printf("----------------------------------------------------------------------\n");
		print_closedports(sys, i);
	}
	free_sys(sys);
	return (0);
}
