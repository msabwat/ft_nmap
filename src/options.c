#include "ft_nmap.h"
#include "libft.h"
#include <stdio.h>
#include <arpa/inet.h>

static void print_usage() {
	printf("Help Screen\n");
	printf("ft_nmap [--help] [--ports [NUM/RANGE]] --ip ADRESSE IP [--speedup [NUM]] [--scan [TYPE]]\n");
	printf("--help Print this help screen\n");
	printf("--ports ports to scan (eg: 1-10 or 1,2,3 or 1,5-15) [1024 ports max]\n");
	printf("--ip ip address to scan\n");
	printf("--file File name containing IP addresses to scan (to use for more than one address)\n");
	printf("--speedup [250 max] number of threads to use\n");
	printf("--scan SYN/NULL/FIN/XMAS/ACK/UDP\n");
	
}

static t_scan_types scan_type_from_str(const char *str) {
	if (ft_strcmp(str, "SYN") == 0)
		return SYN_SCAN;
	else if (ft_strcmp(str, "NULL") == 0)
		return NULL_SCAN;
	else if (ft_strcmp(str, "XMAS") == 0)
		return XMAS_SCAN;
	else if (ft_strcmp(str, "FIN") == 0)
		return FIN_SCAN;
	else if (ft_strcmp(str, "ACK") == 0)
		return ACK_SCAN;
	else if (ft_strcmp(str, "UDP") == 0)
		return UDP_SCAN;
	return (0);
}

static int is_valid_scan_type(char *result, const char *scan_type_name, t_scan_types scan_type) {
	t_scan_types tmp = scan_type_from_str(scan_type_name);
	if (ft_strcmp(result, scan_type_name) == 0) {
		if (((((scan_type & tmp) & tmp) == tmp) && (scan_type != 0)) || (scan_type == tmp)) {
			printf("cannot have the same scan type twice: %s\n", result);
			return(-1);
		}
		return (1);
	}
	return (0);
}

static t_scan_types validate_scan_type(char *result, t_scan_types scan_type) {
	int scan_types_num = 6;
	const char *scan_types[] = {"SYN", "NULL", "FIN", "XMAS", "UDP", "ACK"};
	int ret = 0;
	for (int i = 0; i < scan_types_num; i++) {
		ret = is_valid_scan_type(result, scan_types[i], scan_type);
		if (ret == 1) {
			return (scan_type | scan_type_from_str(scan_types[i]));
		}
		else if (ret == -1) {
			break ;
		}
		// else continue
	}
	if (ret != -1) {
		printf("error: %s is not a valid scan type\n", result);
	}
	return (0);
}

static int get_scan_option(t_sys *sys, char *opt) {
	char *scan_separator = ft_strchr(opt, '/');
	if (scan_separator == NULL) {
		sys->scan_types = validate_scan_type(opt, sys->scan_types);
		if (sys->scan_types == 0) {
			return (0);
		}
	}
	else {
		char cur = *scan_separator;
		char nxt = *(scan_separator + 1);
		if ((cur == opt[0])
			|| (cur == opt[ft_strlen(opt) - 1])
			|| (cur == nxt)) {
			printf("invalid --scan argument\n");
			return (0);
		}
		char **result = ft_strsplit(opt, '/');
		char **tmp = result;
		if (!result) {
			printf("error: no memory\n");
			return (0);
		}
		while (*result != NULL) {
			sys->scan_types = validate_scan_type(*result, sys->scan_types);
			if (sys->scan_types == 0) {
				free_strsplit(tmp);
				return (0);
			}
			result++;
		}
		free_strsplit(tmp);
	}
	return (1);
}

static int handle_ports_range(t_sys *sys, char *arg, int *count) {
	char **range = ft_strsplit(arg, '-');
	if (!range) {
		return (0);
	}
	if (range[2] != NULL) {
		free_strsplit(range);
		return (0);
	}
	unsigned int low = 0;
	unsigned int high = 0;
	if ((ft_atoi(range[0]) >= 1) && (ft_atoi(range[0]) <= 65535)) {
		sys->ports[*count] = ft_atoi(range[0]);
		low = ft_atoi(range[0]);
		*count = *count + 1;
	}
	else {
		free_strsplit(range);
		return (0);
	}
	if ((ft_atoi(range[1]) >= 1) && (ft_atoi(range[1]) <= 65535)) {
		if (ft_atoi(range[1]) - low < 1024) {
			high = ft_atoi(range[1]);
			unsigned int j = low + 1;
			while (j <= high) {
				sys->ports[*count] = j;
				*count = *count + 1;
				j++;
			}
		}
		else {
			free_strsplit(range);
			return (0);
		}
	}
	free_strsplit(range);
	return (1);
}

int get_options(t_sys *sys, int ac, char **av) {
	int mandatory = 0;
	if (ac == 1) {
		printf("no arguments provided, --help to see the usage!\n");
		return (0);
	}
	else {
		for (int i = 1; i < ac; i++) {
			if ((ft_strcmp(av[i], "--help") == 0) && (ac == 2))
				print_usage();
			else if (ft_strcmp(av[i], "--ip") == 0) {
				if (mandatory == 1) {
					printf("error: mandatory option already set!\n");
					return (0);
				}
				if (av[i + 1] == NULL) {
					printf("error: empty ip address\n");
					return (0);
				}
				sys->ip_targets = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in) * 1);
				if (inet_pton(AF_INET, av[i + 1], &(sys->ip_targets[0].sin_addr)) == 1) {
					mandatory = 1;
				}
				else {
					printf("%s: is not a valid ip address!\n", av[i + 1]);
					return(0);
				}
				sys->ip_num = 1;
				i++;
			}
			else if (ft_strcmp(av[i], "--file") == 0) {
				if (mandatory == 1) {
					printf("error: mandatory option already set!\n");
					return (0);
				}
				FILE *fd = fopen(av[i + 1], "r");
				char *buffer = NULL;
				mandatory = 1;
				if (fd == NULL) {
					printf("error: could not open %s\n", av[i + 1]);
					return (0);
				}
				int ret = get_next_line(fileno(fd), &buffer);
				if (ret == -1) {
					printf("error: could not read :%s file content\n", av[i + 1]);
					return (0);
				}
				char **ips = ft_strsplit(buffer, ',');
				if (!ips) {
					printf("error: file :%s not correctly formatted, need a comma separated line\n", av[i + 1]);
					free(buffer);
					return (0);
				}
				char **tmp = ips;
				while (*tmp) {
					sys->ip_num++;
					tmp++;
				}
				sys->ip_targets = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in) * sys->ip_num);
				for (int j = 0; j < sys->ip_num; j++) {
					if (inet_pton(AF_INET, ips[j], &sys->ip_targets[j].sin_addr) == 1) {
						mandatory = 1;
					}
					else {
						printf("in %s: %s is not a valid ip address!\n", av[i + 1], ips[j]);
						free_strsplit(ips);
						free(buffer);
						return(0);
					}
				}
				free_strsplit(ips);
				free(buffer);
				i++;
			}
			else if (ft_strcmp(av[i], "--scan") == 0) {
				if (av[i + 1] == NULL) {
					printf("error: empty --scan option\n");
					return(0);
				}
				if (!get_scan_option(sys, av[i + 1])) {
					return(0);
				}
				i++;
			}
			else if (ft_strcmp(av[i], "--ports") == 0) {
				if (av[i + 1] == NULL) {
					printf("error: empty --ports option\n");
					return(0);
				}
				int count = 0;
				char *ports_separator = ft_strchr(av[i + 1], ',');
				char *range_separator = ft_strchr(av[i + 1], '-');
				if (ports_separator != NULL) {
					char cur = *ports_separator;
					char nxt = *(ports_separator + 1);
					if ((cur == av[i + 1][0])
						|| (cur == av[i + 1][ft_strlen(av[i + 1]) - 1])
						|| (cur == nxt)) {
						printf("error: %s invalid --port argument\n", av[i + 1]);
						return (0);
					}
					char **ports = ft_strsplit(av[i + 1], ',');
					char **tofree = ports;
					if (!ports) {
						printf("error: %s strsplit returned NULL ENOMEM?\n", av[i + 1]);
						return (0);
					}
					char *tmp_ports_separator = NULL;
					while (*ports) {
						tmp_ports_separator = ft_strchr(*ports, '-');
						if (tmp_ports_separator != NULL) {
							char cur = *tmp_ports_separator;
							char nxt = *(tmp_ports_separator + 1);
							if ((cur == (*ports)[0])
								|| (cur == (*ports)[ft_strlen(*ports) - 1])
								|| (cur == nxt)) {
								printf("error: %s invalid --port argument\n", *ports);
								free_strsplit(tofree);
								return (0);
							}
							if (!handle_ports_range(sys, *ports, &count)) {
								printf("error: %s invalid port range\n", *ports);
								free_strsplit(tofree);
								return (0);
							}
						}
						else {
							if ((ft_atoi(*ports) >= 1) && (ft_atoi(*ports) <= 65535)) {
								sys->ports[count] = ft_atoi(*ports);
								count++;
							}
							else {
								printf("error: %s is not a valid port\n", *ports);
								free_strsplit(ports);
								return (0);
							}
						}
						ports++;
					}
					free_strsplit(tofree);
				}
				else if ((range_separator != NULL) && (ports_separator == NULL)) {
					char cur = *range_separator;
					char nxt = *(range_separator + 1);
					if ((cur == av[i + 1][0])
						|| (cur == av[i + 1][ft_strlen(av[i + 1]) - 1])
						|| (cur == nxt)) {
						printf("error: %s invalid --port argument\n", av[i + 1]);
						return (0);
					}
					if (!handle_ports_range(sys, av[i + 1], &count)) {
						printf("error: %s invalid port range\n", av[i + 1]);
						return (0);
					}
				}
				else {
					if ((ft_atoi(av[i + 1]) >= 1) && (ft_atoi(av[i + 1]) <= 65535)) {
						sys->ports[count] = ft_atoi(av[i + 1]);
					}
					else {
						printf("error: %s is not a valid port\n", av[i + 1]);
						return (0);
					}
				}
				i++;
			}
			else if (ft_strcmp(av[i], "--speedup") == 0) {
				if (av[i + 1] == NULL) {
					printf("error: empty --speedup option\n");
					return(0);
				}
				if (!((ft_atoi(av[i + 1]) >= 1) && (ft_atoi(av[i + 1]) <= 250))) {
					printf("%s: invalid thread number!\n", av[i + 1]);
					return (0);
				}
				sys->speed = ft_atoi(av[i + 1]);
				i++;
			}
			else {
				printf("invalid options, --help to see the usage!\n");
				return (0);
			}
		}
		if (mandatory == 0) {
			printf("error: missing mandatory argument, --help to see the usage\n");
			return (0);
		}	
		return (1);
	}
}
