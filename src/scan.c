#include "ft_nmap.h"
#include "libft.h"
#include <stdio.h>
#include <pthread.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pcap/sll.h>

int init_scan(t_sys *sys) {
	// init mutex si (speed != 0)
	sys->scans = (t_scan *)malloc(sizeof(t_scan) * (sys->scan_num + 1));
	if (!sys->scans) {
		perror("allocation error: ");
		return (0);
	}
	int scan_num = 0;
	t_scan_types types = sys->scan_types;
	t_scan_types type = 0;
	for (int i = 0; i < sys->ip_num; i++) {
		for (int j = 0; j < sys->ports_num; j++) {
			sys->scans[scan_num].dst_ip = sys->ip_targets[i];
			sys->scans[scan_num].dst_port = sys->ports[j];
			sys->scans[scan_num].results = (t_result *)malloc(sizeof(t_result) * sys->scan_types_num);
			if (!sys->scans[scan_num].results) {
				perror("allocation error: ");
				return (0);
			}
			types = sys->scan_types;
			type = 0;
			for (int k = 0; k < sys->scan_types_num; k++) {
				if (((types & SYN_SCAN) & SYN_SCAN) == SYN_SCAN) {
					type = SYN_SCAN;
					types -= SYN_SCAN;
				}
				else if (((types & NULL_SCAN) & NULL_SCAN) == NULL_SCAN) {
					type = NULL_SCAN;
					types -= NULL_SCAN;
				}
				else if (((types & FIN_SCAN) & FIN_SCAN) == FIN_SCAN) {
					type = FIN_SCAN;
					types -= FIN_SCAN;
				}
				else if (((types & UDP_SCAN) & UDP_SCAN) == UDP_SCAN) {
					type = UDP_SCAN;
					types -= UDP_SCAN;
				}
				else if (((types & ACK_SCAN) & ACK_SCAN) == ACK_SCAN) {
					type = ACK_SCAN;
					types -= ACK_SCAN;
				}
				else if (((types & XMAS_SCAN) & XMAS_SCAN) == XMAS_SCAN) {
					type = XMAS_SCAN;
					types -= XMAS_SCAN;
				}
				sys->scans[scan_num].results[k].scan_type = type;
				sys->scans[scan_num].results[k].status = INVALID;
				sys->scans[scan_num].results[k].service_name = NULL;
			}
			scan_num++;
		}
	}
	if (sys->speed) {
		sys->task_count = sys->scan_num;
		pthread_mutex_init(&(sys->m_task_count), NULL);
		sys->ids = (pthread_t *)malloc(sizeof(pthread_t) * sys->speed);
		if (!sys->ids) {
			perror("allocation error: ");
			return (0);
		}
	}
	return (1);
}

int send_scan_pkt(t_sys *sys, t_scan *scan, unsigned int src_port, t_scan_types scan_type) {
	if (scan_type == UDP_SCAN) {
		int sock = socket (AF_INET, SOCK_DGRAM , IPPROTO_UDP);
		if (sock == -1) {
			return (0);
		}
		char buffer[4096];
		ft_memset (buffer, 0, 4096);
		struct sockaddr_in  dest;
		dest.sin_family = AF_INET;
		dest.sin_port = htons(scan->dst_port);
		dest.sin_addr.s_addr = scan->dst_ip.sin_addr.s_addr;
		struct sockaddr_in src;
		src.sin_family = AF_INET;
		src.sin_port = htons(src_port);
		src.sin_addr.s_addr = inet_addr(sys->src_ip);
		if (bind(sock, (struct sockaddr *) &src,
				 sizeof(struct sockaddr_in)) == -1) {
			//printf("failed to bind source address %d\n", src_port);
			return (-1);
		}
		if ( sendto (sock, buffer , sizeof(struct iphdr) + sizeof(struct udphdr) ,
					 0, (struct sockaddr *) &dest, sizeof (dest)) < 0 ) {
			return (0);
		}
		close(sock);
	}
	else {
		int sock = socket (AF_INET, SOCK_RAW , IPPROTO_TCP);
		if (sock == -1) {
			return (0);
		}

		int one = 1;
		const int *val = &one;		
		if (setsockopt (sock, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0) {
			return (0);
		}
		
		char buffer[4096]; 
		struct iphdr *iph = (struct iphdr *) buffer;		
		struct tcphdr *tcph = (struct tcphdr *) (buffer + sizeof (struct iphdr));
	
		struct sockaddr_in  dest;
		t_pseudo_header psh;
		
		struct in_addr dest_ip;
		dest_ip.s_addr = scan->dst_ip.sin_addr.s_addr;
		ft_memset (buffer, 0, 4096);
		
		iph->ihl = 5;
		iph->version = 4;
		iph->tos = 0;
		iph->tot_len = sizeof (struct iphdr) + sizeof (struct tcphdr);
		iph->id = htons (getpid() & 0xFFFF);
		iph->frag_off = htons(16384);
		iph->ttl = 64;
		iph->protocol = IPPROTO_TCP;
		iph->check = 0;
		iph->saddr = inet_addr (sys->src_ip);
		iph->daddr = dest_ip.s_addr;
		iph->check = checksum ((unsigned short *) buffer, iph->tot_len >> 1);
 
		tcph->source = htons (src_port);
		tcph->seq = htons(0);
		tcph->ack_seq = 0;
		tcph->doff = sizeof(struct tcphdr) / 4;
		tcph->fin=0;
		tcph->syn=0;
		tcph->rst=0;
		tcph->psh=0;
		tcph->ack=0;
		tcph->urg=0;
		if (scan_type == SYN_SCAN)
			tcph->syn = 1;
		else if (scan_type == ACK_SCAN)
			tcph->ack = 1;
		else if (scan_type == FIN_SCAN)
			tcph->fin = 1;
		else if (scan_type == XMAS_SCAN) {
			tcph->fin=1;
			tcph->psh=1;
			tcph->urg=1;
		}
		else if (scan_type == NULL_SCAN)
			(void)0;
		tcph->window = htons ( 14600 );
		tcph->check = 0;
		tcph->urg_ptr = 0;

		dest.sin_family = AF_INET;
		dest.sin_port = htons (scan->dst_port);
		dest.sin_addr.s_addr = dest_ip.s_addr;
		tcph->dest = htons (scan->dst_port);
		tcph->check = 0;
		psh.source_address = inet_addr(sys->src_ip);
		psh.dest_address = dest.sin_addr.s_addr;
		psh.placeholder = 0;
		psh.protocol = IPPROTO_TCP;
		psh.tcp_length = htons( sizeof(struct tcphdr) );

		ft_memcpy(&psh.tcp , tcph , sizeof (struct tcphdr));
 
		tcph->check = checksum( (unsigned short*) &psh , sizeof (t_pseudo_header));
		
		if ( sendto (sock, buffer , sizeof(struct iphdr) + sizeof(struct tcphdr) ,
					 0, (struct sockaddr *) &dest, sizeof (dest)) < 0 ) {
			return (0);
		}
		close(sock);
	}
	// tout va bien
	return (1);
}

t_scan *find_scan_entry(t_sys *sys, unsigned long ip, unsigned int port) {
	for (int i = 0; i < sys->scan_num; i++) {
		if (sys->scans[i].dst_ip.sin_addr.s_addr == ip) {
			for (int j = i; j < sys->scan_num; j++) {
				if (sys->scans[j].dst_port == port) {
					return (&sys->scans[j]);
				}
			}
		}
	}
	// something went very wrong (this should never happen)
	printf("discarding packet from ip:%s port:%d \n", inet_ntoa( (struct in_addr){ .s_addr = ip }), port);
	return (NULL);
	
}

void add_result(t_sys *sys, int protocol, unsigned long ip,
				unsigned int port, const u_char *pkt) {
	t_scan *scan = NULL;
	if (protocol == IPPROTO_TCP) {
		scan = find_scan_entry(sys, ip, port);
		if (!scan) {
			return ;
		}
		t_scan_types type = scan->results[sys->current_dispatch].scan_type;
		struct iphdr *ip_response = (struct iphdr *) (pkt + sizeof(struct sll_header));
		struct tcphdr *tcp_response = (struct tcphdr *)(pkt + sizeof(struct sll_header) + (ip_response->ihl * 4));
		if (type == SYN_SCAN) {
			if ((tcp_response->ack == 1) && (tcp_response->syn == 1)) {
				scan->results[sys->current_dispatch].status = OPEN;
				printf(".");
			}
			else if (tcp_response->rst == 1) {
				scan->results[sys->current_dispatch].status = CLOSED;
				printf(".");
			}
		}
		else if ((type == NULL_SCAN) || (type == FIN_SCAN) || (type == XMAS_SCAN)) {
			if (tcp_response->rst == 1) {
				scan->results[sys->current_dispatch].status = CLOSED;
				printf(".");
			}
		}
		else if (type == ACK_SCAN) {
			if (tcp_response->rst == 1) {
				scan->results[sys->current_dispatch].status = UNFILTERED;
				printf(".");
			}
		}
	}
	else if (protocol == IPPROTO_ICMP) {
		struct iphdr *ip_response = (struct iphdr *) (pkt + sizeof(struct sll_header));
		struct icmphdr *icmp_response = (struct icmphdr *)(pkt + sizeof(struct sll_header) + (ip_response->ihl * 4));
		struct iphdr *ip_source = (struct iphdr *) (pkt + sizeof(struct sll_header) + sizeof(struct iphdr) + sizeof(struct icmphdr));
		// find source port after the ICMP response header
		if (ip_source->protocol == IPPROTO_UDP) {
			struct udphdr *udp_source = (struct udphdr *)(((char *)ip_source) + sizeof(struct iphdr));
			scan = find_scan_entry(sys, ip, ntohs(udp_source->dest));
			if (!scan)
				return ;
			t_scan_types type = scan->results[sys->current_dispatch].scan_type;
			if (type == UDP_SCAN) {
				if ((icmp_response->type == 3) && (icmp_response->code == 3)) {
					scan->results[sys->current_dispatch].status = CLOSED;
					printf(".");
				}
				else if ((icmp_response->type == 3) &&
						 ((icmp_response->code == 1) || (icmp_response->code == 2) || (icmp_response->code == 9) ||
						  (icmp_response->code == 10) || (icmp_response->code == 13))) {
					scan->results[sys->current_dispatch].status = FILTERED;
					printf(".");
				}
			}
		}
		else if (ip_source->protocol == IPPROTO_TCP) {
			struct tcphdr *tcp_source = (struct tcphdr *)(((char *)ip_source) + sizeof(struct iphdr));
			scan = find_scan_entry(sys, ip, ntohs(tcp_source->dest));
			if (!scan)
				return ;
			// this is valid for all scan types
			// t_scan_types type = scan->results[sys->current_dispatch].scan_type;
			if ((icmp_response->type == 3) &&
				((icmp_response->code == 1) || (icmp_response->code == 2) || (icmp_response->code == 9) ||
				 (icmp_response->code == 10) || (icmp_response->code == 13))) {
				scan->results[sys->current_dispatch].status = FILTERED;
				printf(".");
			}
		}
	}
	else if (protocol == IPPROTO_UDP) {
		scan = find_scan_entry(sys, ip, port);
		// unusual
		scan->results[sys->current_dispatch].status = OPEN;
		printf(".");
	}
	else {
		printf("received packet from unknown protocol, discarding...\n");
	}
	fflush(stdout);
}
