#include <pthread.h>
#include "ft_nmap.h"
#include "libft.h"

void *syn_task_runner(void *args) {
	t_sys *sys = (t_sys *)args;
	int task = 0;
	int count = 1;
	while (42) {
		pthread_mutex_lock(&(sys->m_task_count));
		task = (sys->scan_num - sys->task_count);
		if (sys->task_count == -1) {
			pthread_mutex_unlock(&(sys->m_task_count));
			break;
		}
		else {
			sys->task_count--;
			pthread_mutex_unlock(&(sys->m_task_count));
			send_scan_pkt(sys, &sys->scans[task], count++, SYN_SCAN);
		}
	}
	pthread_exit(0);
}

void *null_task_runner(void *args) {
	t_sys *sys = (t_sys *)args;
	int task = 0;
	int count = 1;
	while (42) {
		pthread_mutex_lock(&(sys->m_task_count));
		task = (sys->scan_num - sys->task_count) + 1;
		if (sys->task_count == 0) {
			pthread_mutex_unlock(&(sys->m_task_count));
			break;
		}
		else {
			sys->task_count--;
			pthread_mutex_unlock(&(sys->m_task_count));
			send_scan_pkt(sys, &sys->scans[task], count++, NULL_SCAN);
		}
	}
	pthread_exit(0);
}

void *ack_task_runner(void *args) {
	t_sys *sys = (t_sys *)args;
	int task = 0;
	int count = 1;
	while (42) {
		pthread_mutex_lock(&(sys->m_task_count));
		task = (sys->scan_num - sys->task_count) + 1;
		if (sys->task_count == 0) {
			pthread_mutex_unlock(&(sys->m_task_count));
			break;
		}
		else {
			sys->task_count--;
			pthread_mutex_unlock(&(sys->m_task_count));
			send_scan_pkt(sys, &sys->scans[task], count++, ACK_SCAN);
		}
	}
	pthread_exit(0);
}

void *fin_task_runner(void *args) {
	t_sys *sys = (t_sys *)args;
	int task = 0;
	int count = 1;
	while (42) {
		pthread_mutex_lock(&(sys->m_task_count));
		task = (sys->scan_num - sys->task_count) + 1;
		if (sys->task_count == 0) {
			pthread_mutex_unlock(&(sys->m_task_count));
			break;
		}
		else {
			sys->task_count--;
			pthread_mutex_unlock(&(sys->m_task_count));
			send_scan_pkt(sys, &sys->scans[task], count++, FIN_SCAN);
		}
	}
	pthread_exit(0);
}

void *xmas_task_runner(void *args) {
	t_sys *sys = (t_sys *)args;
	int task = 0;
	int count = 1;
	while (42) {
		pthread_mutex_lock(&(sys->m_task_count));
		task = (sys->scan_num - sys->task_count) + 1;
		if (sys->task_count == 0) {
			pthread_mutex_unlock(&(sys->m_task_count));
			break;
		}
		else {
			sys->task_count--;
			pthread_mutex_unlock(&(sys->m_task_count));
			send_scan_pkt(sys, &sys->scans[task], count++, XMAS_SCAN);
		}
	}
	pthread_exit(0);
}

void *udp_task_runner(void *args) {
	t_sys *sys = (t_sys *)args;
	int task = 0;
	int tmp = task;
	while (42) {
		pthread_mutex_lock(&(sys->m_task_count));
		task = (sys->scan_num - sys->task_count) + 1;
		if (sys->task_count == 0) {
			pthread_mutex_unlock(&(sys->m_task_count));
			break;
		}
		else {
			sys->task_count--;
			pthread_mutex_unlock(&(sys->m_task_count));
			int ret = 0;
			ret = send_scan_pkt(sys, &sys->scans[task], tmp, UDP_SCAN);
			while (ret == -1) {
				ret = send_scan_pkt(sys, &sys->scans[task], tmp, UDP_SCAN);
				tmp++;
			}
		}
	}
	pthread_exit(0);
}
