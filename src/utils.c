#include "ft_nmap.h"
#include "libft.h"
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>

void print_scanconf(t_sys *sys) {
	char psz_ip[sys->ip_num][16];
	printf("Scan configuration:\n");
	printf("Target IP-Address : ");
	for (int i = 0; i < sys->ip_num; i++) {
		if (!inet_ntop(AF_INET, &sys->ip_targets[i].sin_addr, psz_ip[i], 16)) {
			printf("fatal error: could not get ip string from sockaddr_in struct\n");
			exit(1);
		}
		printf("%s ", psz_ip[i]);
	}
	printf("\n");

	for (int i = 0; i < 1024; i++) {
		if (sys->ports[i] != 0)
			sys->ports_num++;
	}
	if (sys->ports_num > 1024) {
		printf("error: cannot scan more than 1024 ports!\n");
		exit(1);
	}
	printf("Number of ports to scan: %d\n", sys->ports_num);

	printf("Scans to be performed : ");
	const char *str_scan_types[] = {"SYN", "NULL", "FIN", "XMAS", "UDP", "ACK"};
	const t_scan_types scan_types[] = { SYN_SCAN, NULL_SCAN, FIN_SCAN, XMAS_SCAN, UDP_SCAN, ACK_SCAN };
	for (int i = 0; i < 6; i++) {
		if (((sys->scan_types & scan_types[i]) & scan_types[i]) == scan_types[i]) {
			printf("%s ", str_scan_types[i]);
			sys->scan_types_num++;
		}
	}
	printf("\n");
	
	printf("Number of threads: %d\n", sys->speed);
}

int get_local_ip ( char * buffer) {
	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
	if (sock == -1) {
		return (0);
	}

	struct timeval tv;
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,
				   (struct timeval *)&tv,sizeof(struct timeval)) < 0) {
		printf("unable to set SO_RCVTIME0 option\n");
		return (0);
	}

	struct icmphdr icmp_hdr;
	unsigned char data[2048];
	ft_memset(&icmp_hdr, 0, sizeof(icmp_hdr));
	icmp_hdr.type = ICMP_ECHO;
	icmp_hdr.un.echo.id = htons(4235);

	struct sockaddr_in dns;
	ft_memset( &dns, 0, sizeof(dns) );
	dns.sin_family = AF_INET;
	dns.sin_addr.s_addr = inet_addr("8.8.8.8");
	ft_memcpy(data, &icmp_hdr, sizeof(icmp_hdr));
	int ret = sendto(sock, data, sizeof(icmp_hdr),
					 0, (struct sockaddr*)&dns, sizeof(dns));
	if (ret == -1) {
		// printf("could not ping google dns server\n");
		return (0);
	}
	close(sock);

	sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sock == -1) {
	 return (0);
	}
	unsigned char recv_data[2048];

	unsigned int len = sizeof(struct sockaddr_in);
	// won't block because of recvtime out
	recvfrom(sock, recv_data, sizeof(struct iphdr) + sizeof(struct icmphdr),
			 0, (struct sockaddr *)&dns, &len);

	struct ip *ip = (struct ip *)(recv_data);
	const char *p = inet_ntop(AF_INET, &ip->ip_dst.s_addr, buffer, 100);
	if (!p) {
		// printf("could not get ip destination \n");
		return (0);
	}
	close(sock);
	return (1);
}

unsigned short checksum(unsigned short *ptr, int nbytes) {
	unsigned int sum = 0;

	for ( sum = 0; nbytes > 1; nbytes -= 2 )
		sum += *ptr++;
	if ( nbytes == 1 )
		sum += *(unsigned char*)ptr;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	return (~sum);
}

int set_pcap_filter(t_sys *sys) {
	struct bpf_program filter;
	char *filter_exp;
	// sizeof (addr) = 16
	// sizeof "src host " = 9
	// sizeof " and " = 5
	// on a recent kernel (> 5.1), the limit is 1 000 000 BPF instructions
	// cf. https://docs.cilium.io/en/v1.9/bpf/
	filter_exp = (char *)malloc( sizeof(char) * (16 + 9) + sizeof(char) * ((5 + 9 + 16) * (sys->ip_num - 1)) + sizeof(char));
	if (!filter_exp) {
		return (0);
	}
	ft_bzero(filter_exp, (16 + 9) + (5 + 9 + 16) * (sys->ip_num - 1) + 1);
	filter_exp[(16 + 9) + (5 + 9 + 16) * (sys->ip_num - 1)] = '\0';
	if (sys->ip_num == 1) {
		ft_strncat(filter_exp, "src host ", 9);
		ft_strncat(filter_exp, inet_ntoa(sys->scans[0].dst_ip.sin_addr), 16);
	}
	else {
		ft_strncat(filter_exp, "src host ", 9);
		ft_strncat(filter_exp, inet_ntoa(sys->scans[0].dst_ip.sin_addr), 16);
		for (int i = 1; i < sys->ip_num; i++) {
			ft_strncat(filter_exp, " and ", 5);
			ft_strncat(filter_exp, "src host ", 9);
			ft_strncat(filter_exp, inet_ntoa(sys->ip_targets[i].sin_addr), 16);
		}
	}
	if (pcap_compile(sys->capture, &filter, filter_exp, 0, 0) == -1) {
		printf("Bad filter - %s\n", pcap_geterr(sys->capture));
		return (0);
	}

	if (pcap_setfilter(sys->capture, &filter) == -1) {
		printf("Error setting filter - %s\n", pcap_geterr(sys->capture));
		return (0);
	}
	free(filter_exp);
	pcap_freecode(&filter);
	return (1);
}

char *str_from_scan_type(t_scan_types type) {
	if (type == SYN_SCAN)
		return "SYN";
	else if (type == NULL_SCAN)
		return "NULL";
	else if (type == XMAS_SCAN)
		return "XMAS";
	else if (type == FIN_SCAN)
		return "FIN";
	else if (type == ACK_SCAN)
		return "ACK";
	else if (type == UDP_SCAN)
		return "UDP";
	return (NULL);
}

char *str_from_result(t_status status) {
	if (status == OPEN)
		return "open";
	else if (status == CLOSED)
		return "closed";
	else if (status == FILTERED)
		return "filtered";
	else if (status == UNFILTERED)
		return "unfiltered";
	else if (status == OPEN_OR_FILTERED)
		return "open|filtered";
	return (NULL);
}

void print_openports(t_sys *sys, int ip_index) {
	int ind = ip_index * sys->ports_num;
	struct servent *app;
	char result[20];
	for (int i = 0; i < sys->ports_num; i++) {
		if (sys->scan_types_num == 1) {
			if (sys->scans[ind + i].results[0].status == OPEN) {
				printf("%-5d | ", sys->scans[ind + i].dst_port);
				app = getservbyport(htons(sys->scans[ind + i].dst_port), NULL);
				if (app)
					printf("%-20s | ", app->s_name);
				else {
					printf("%-20s | ", "Unassigned");
				}
				ft_bzero(result, 20);
				ft_strncat(result,str_from_scan_type(sys->scans[ind + i].results[0].scan_type),
						   ft_strlen(str_from_scan_type(sys->scans[ind + i].results[0].scan_type)));
				ft_strncat(result, "(", 1);
				ft_strncat(result,str_from_result(sys->scans[ind + i].results[0].status),
						   ft_strlen(str_from_result(sys->scans[ind + i].results[0].status)));
				ft_strncat(result, ")", 1);
				printf("%*s | %-10s\n", 20, result, "Open");
			}
		}
		else {
			int flag = 0;
			for (int k = 0; k < sys->scan_types_num; k++) {
				if (sys->scans[ind + i].results[k].status == OPEN) {
					flag = 1;
				}
			}
			if (flag == 1) {
				printf("%-5d | ", sys->scans[ind + i].dst_port);
				app = getservbyport(htons(sys->scans[ind + i].dst_port), NULL);
				if (app)
					printf("%-20s | ", app->s_name);
				else {
					printf("%-20s | ", "Unassigned");
				}
				ft_bzero(result, 20);
				ft_strncat(result,str_from_scan_type(sys->scans[ind + i].results[0].scan_type),
						   ft_strlen(str_from_scan_type(sys->scans[ind + i].results[0].scan_type)));
				ft_strncat(result, "(", 1);
				ft_strncat(result,str_from_result(sys->scans[ind + i].results[0].status),
						   ft_strlen(str_from_result(sys->scans[ind + i].results[0].status)));
				ft_strncat(result, ")", 1);
				printf("%*s | %-10s\n", 20, result, "Open");
				for (int k = 1; k < sys->scan_types_num; k++) {
					printf("%-5s | %-20s | ", " ", " ");
					ft_bzero(result, 20);
					ft_strncat(result,str_from_scan_type(sys->scans[ind + i].results[k].scan_type),
							   ft_strlen(str_from_scan_type(sys->scans[ind + i].results[k].scan_type)));
					ft_strncat(result, "(", 1);
					ft_strncat(result,str_from_result(sys->scans[ind + i].results[k].status),
							   ft_strlen(str_from_result(sys->scans[ind + i].results[k].status)));
					ft_strncat(result, ")", 1);
					printf("%*s | %-10s\n", 20, result, " ");
				}
			}
		}
	}
}

void print_closedports(t_sys *sys, int ip_index) {
	(void)sys, (void) ip_index;
	int ind = ip_index * sys->ports_num;
	struct servent *app;
	char result[20];
	for (int i = 0; i < sys->ports_num; i++) {
		if (sys->scan_types_num == 1) {
			if (sys->scans[ind + i].results[0].status != OPEN) {
				printf("%-5d | ", sys->scans[ind + i].dst_port);
				app = getservbyport(htons(sys->scans[ind + i].dst_port), NULL);
				if (app)
					printf("%-20s | ", app->s_name);
				else {
					printf("%-20s | ", "Unassigned");
				}
				ft_bzero(result, 20);
				ft_strncat(result,str_from_scan_type(sys->scans[ind + i].results[0].scan_type),
						   ft_strlen(str_from_scan_type(sys->scans[ind + i].results[0].scan_type)));
				ft_strncat(result, "(", 1);
				ft_strncat(result,str_from_result(sys->scans[ind + i].results[0].status),
						   ft_strlen(str_from_result(sys->scans[ind + i].results[0].status)));
				ft_strncat(result, ")", 1);
				if (sys->scans[ind + i].results[0].status == CLOSED)
					printf("%*s | %-10s\n", 20, result, "Closed");
				else if ((sys->scans[ind + i].results[0].status == FILTERED) ||
						 (sys->scans[ind + i].results[0].status == OPEN_OR_FILTERED))
					printf("%*s | %-10s\n", 20, result, "Filtered");
				else if (sys->scans[ind + i].results[0].status == UNFILTERED)
					printf("%*s | %-10s\n", 20, result, "Unfiltered");
			}
		}
		else {
			int flag = 0;
			for (int k = 0; k < sys->scan_types_num; k++) {
				if (sys->scans[ind + i].results[k].status == OPEN) {
					flag = 1;
				}
			}
			if (flag == 0) {
				printf("%-5d | ", sys->scans[ind + i].dst_port);
				app = getservbyport(htons(sys->scans[ind + i].dst_port), NULL);
				if (app)
					printf("%-20s | ", app->s_name);
				else {
					printf("%-20s | ", "Unassigned");
				}
				ft_bzero(result, 20);
				ft_strncat(result,str_from_scan_type(sys->scans[ind + i].results[0].scan_type),
						   ft_strlen(str_from_scan_type(sys->scans[ind + i].results[0].scan_type)));
				ft_strncat(result, "(", 1);
				ft_strncat(result,str_from_result(sys->scans[ind + i].results[0].status),
						   ft_strlen(str_from_result(sys->scans[ind + i].results[0].status)));
				ft_strncat(result, ")", 1);
				printf("%*s |", 20, result);
				int status = 0;
				for (int k = 0; k < sys->scan_types_num; k++) {
					if (sys->scans[ind + i].results[k].status == CLOSED) {
						status = 1;
						break;
					}
				}
				if (status == 1) {
					printf(" %-10s\n", "Closed");
				}
				else if (status == 0) {
					printf(" %-10s\n", "Filtered or Unfiltered");
				}
				for (int k = 1; k < sys->scan_types_num; k++) {
					printf("%-5s | %-20s | ", " ", " ");
					ft_bzero(result, 20);
					ft_strncat(result,str_from_scan_type(sys->scans[ind + i].results[k].scan_type),
							   ft_strlen(str_from_scan_type(sys->scans[ind + i].results[k].scan_type)));
					ft_strncat(result, "(", 1);
					ft_strncat(result,str_from_result(sys->scans[ind + i].results[k].status),
							   ft_strlen(str_from_result(sys->scans[ind + i].results[k].status)));
					ft_strncat(result, ")", 1);
					printf("%*s | %-10s\n", 20, result, " ");
				}
			}
		}
	}
}

void free_sys(t_sys *sys) {
	if (sys->ip_targets) {
		free(sys->ip_targets);
	}
	if (sys->ids) {
		free(sys->ids);
	}
	for (int i = 0; i < sys->scan_num; i++) {
		if (sys->scans[i].results) {
			free(sys->scans[i].results);
		}
	}
	if (sys->scans) {
		free(sys->scans);
	}
	if (sys)
		free(sys);
}

void free_strsplit(char **str) {
	if (!str)
		return ;
	char **tmp = str;
	while (*str != NULL) {
		free(*str);
		str++;
	}
	free(tmp);
}
