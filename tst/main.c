#include <stdlib.h>
#include <stdio.h>

typedef enum e_opts {
	OPTION1 = 1,
	OPTION2 = 1 << 1,
	OPTION3 = 1 << 2,
	OPTION4 = 1 << 3,
	OPTION5 = 1 << 4,
} t_opts;

main() {
	t_opts opts = OPTION5 | OPTION3;
	if ((opts & OPTION1) != 0)
		printf("option 1 set\n");
	if ((opts & OPTION2) != 0)
		printf("option 2 set\n");
	if ((opts & OPTION3) != 0)
		printf("option 3 set\n");
	if ((opts & OPTION4) != 0)
		printf("option 4 set\n");
	if ((opts & OPTION5) != 0)
		printf("option 5 set\n");
}
