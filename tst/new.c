#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static size_t	ft_isword(char const *str, int i, char delim) {
	if (i == 0)
		return (0);
	if ((str[i] == delim || str[i] == '\0')
		&& (str[i - 1] != delim && str[i - 1] != '\0'))
		return (1);
	return (0);
}

static size_t	ft_cntdelimwords(char const *str, char d) {
	int	j;
	int	cnt;

	cnt = 0;
	j = 0;
	while (str[j])
	{
		if (ft_isword(str, j, d))
			cnt++;
		j++;
	}
	if (ft_isword(str, j, d))
		cnt++;
	return (cnt);
}

static char	*get_prevword(char *s, size_t index, char delim) {
	char	*temp;
	int		x;
	int		cnt;

	x = index - 1;
	cnt = 0;
	while (x >= 0 && s[x] != delim)
		x--;
	x++;
	temp = (char *)malloc(sizeof(char) * ((int)index - x + 1));
	if (!temp)
		return (NULL);
	while (x < (int)index)
	{
		temp[cnt] = s[x];
		cnt++;
		x++;
	}
	temp[cnt] = '\0';
	return (temp);
}

char		**ft_strsplit(char const *s, char c) {
	char	**result;
	int		ind;
	int		cntr1;

	ind = 0;
	cntr1 = 0;
	if (!s)
		return (NULL);
	result = (char **)malloc(sizeof(char *) * (ft_cntdelimwords(s, c) + 1));
	if (!result)
		return (NULL);
	while (s[ind])
	{
		if (ft_isword((char *)s, ind, c))
		{
			result[cntr1] = get_prevword((char *)s, ind, c);
			cntr1++;
		}
		ind++;
	}
	if (ft_isword((char *)s, ind, c))
		result[cntr1++] = get_prevword((char *)s, ind, c);
	result[cntr1] = 0;
	return (result);
}

int main(int ac, char **av) {
	if (ac == 2) {
		if (strchr(av[1], '/') == NULL)
			abort();
		else {
			char c = *(strchr(av[1], '/'));
			char next = *(strchr(av[1], '/') + 1);
			if ((c == av[1][0]) || (c == av[1][strlen(av[1]) - 1]) || (c == next))
				abort();
		}
		char **result = ft_strsplit(av[1], '/');
		while (*result != NULL) {
			// SYN, NULL, ACK, FIN, XMAS, UDP
			result++;
		}
	}
	return(0);
}
