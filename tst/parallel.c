#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

typedef struct s_sys {
	int task_count;
	pthread_mutex_t m_task_count;
	struct s_list *head;
} t_sys;

typedef struct s_list {
	pthread_t 		tid;
	// add struct to store the scan result
	t_sys *sys;
	struct s_list	*next;
} t_list;


static void run_task(int task) {
	printf("running task %d\n", task);
	int randomTime = rand() % 2;
	sleep(randomTime);
//	sleep(1);
}

static void *run_parallel_task(void *args) {
	t_list *tmp = (t_list *)args;
	int randomTime = 0;
	int task = 0;
	while (42) {
		pthread_mutex_lock(&(tmp->sys->m_task_count));
		task = tmp->sys->task_count;
		if (task == 0) {
			// do nothing
			(void) 0;
			pthread_mutex_unlock(&(tmp->sys->m_task_count));
			break;
		}
		else {
			tmp->sys->task_count--;
			pthread_mutex_unlock(&(tmp->sys->m_task_count));
			run_task(task);
		}
	}
	pthread_exit(0);
}

int main(int ac, char **av) {
	if (ac != 3)
		return (1);
	t_sys *sys = (t_sys *)malloc(sizeof(t_sys));
	if (!sys)
		return (1);

	sys->head = (t_list *)malloc(sizeof(t_list));
	if (!sys->head)
		return (1);

	sys->task_count = atoi(av[1]);
	sys->head->tid = 0;
	sys->head->next = NULL;
	pthread_mutex_init(&(sys->m_task_count), NULL);
	sys->head->sys = sys;

	t_list *tmp = sys->head;
	if (atoi(av[2]) == 0) {
		for (int i = 1; i <= atoi(av[1]); i++) {
			run_task(sys->task_count--);
		}
	}
	else if (atoi(av[2]) > 0) {
		for (int i = 1; i <= atoi(av[2]); i++) {
			pthread_create(&(tmp->tid), NULL, &run_parallel_task, tmp);
			tmp->next = (t_list *)malloc(sizeof(t_list));
			tmp->next->tid = 0;
			tmp->next->sys = sys;
			tmp->next->next = NULL;
			tmp = tmp->next;
		}
	}
	if (atoi(av[2]) > 0) {
		tmp = sys->head;
		// join thee threads (not a typo!!)
		for (int i = 1; i <= atoi(av[2]); i++) {
			pthread_join(tmp->tid, NULL);
			tmp = tmp->next;
		}
	}
	
	return (0);
}
