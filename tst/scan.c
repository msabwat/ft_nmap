#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef enum e_scan_types {
	SYN_SCAN = 1,
	NULL_SCAN = 1 << 1,
	ACK_SCAN = 1 << 2,
	FIN_SCAN = 1 << 3,
	XMAS_SCAN = 1 << 4,
	UDP_SCAN = 1 << 5
} t_scan_types;

main() {
	t_scan_types scan_type = NULL_SCAN | SYN_SCAN;
	printf("1: %d\n", SYN_SCAN);
	printf("2: %d\n", NULL_SCAN);
	printf("test: %d\n", (scan_type ^ NULL_SCAN));
	printf("%d\n", 0b00000001 ^ 0b00000010);
	assert(0b00000001 == SYN_SCAN);
	assert(0b00000010 == NULL_SCAN);
	assert(0b00000100 == ACK_SCAN);
	assert(0b00001000 == FIN_SCAN);
	assert(0b00010000 == XMAS_SCAN);
	assert(0b00100000 == UDP_SCAN);
	
	assert(scan_type == 0b00000011);
	assert(((scan_type & NULL_SCAN) & NULL_SCAN));
}
